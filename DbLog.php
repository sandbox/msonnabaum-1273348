<?php
class DbLog implements WatchdogInterface {

  public function getMessageTypes() {
    $types = array();

    $result = db_query('SELECT DISTINCT(type) FROM {watchdog} ORDER BY type');
    foreach ($result as $object) {
      $types[] = $object->type;
    }

    return $types;
  }
  /**
   * Implements hook_watchdog().
   *
   * Note some values may be truncated for database column size restrictions.
   */
  function log(array $log_entry) {
    // The user object may not exist in all conditions, so 0 is substituted if needed.
    $user_uid = isset($log_entry['user']->uid) ? $log_entry['user']->uid : 0;

    Database::getConnection('default', 'default')->insert('watchdog')
        ->fields(array(
                      'uid' => $user_uid,
                      'type' => substr($log_entry['type'], 0, 64),
                      'message' => $log_entry['message'],
                      'variables' => serialize($log_entry['variables']),
                      'severity' => $log_entry['severity'],
                      'link' => substr($log_entry['link'], 0, 255),
                      'location' => $log_entry['request_uri'],
                      'referer' => $log_entry['referer'],
                      'hostname' => substr($log_entry['ip'], 0, 128),
                      'timestamp' => $log_entry['timestamp'],
                 ))
        ->execute();
  }
  public function get($wid) {
    $result = db_query('SELECT w.*, u.name, u.uid FROM {watchdog} w INNER JOIN {users} u ON w.uid = u.uid WHERE w.wid = :id', array(':id' => $wid))->fetchObject();
    return $result ?: FALSE;
  }
  public function getMultiple($limit = 50, $sort_field = 'wid', $sort_direction = 'DESC') {
    $query = db_select('watchdog', 'w')->extend('PagerDefault')->extend('TableSort');
    $query->leftJoin('users', 'u', 'w.uid = u.uid');
    $query
        ->fields('w', array('wid', 'uid', 'severity', 'type', 'timestamp', 'message', 'variables', 'link'))
        ->addField('u', 'name');
    if (!empty($filter['where'])) {
      $query->where($filter['where'], $filter['args']);
    }
    return $query
        ->limit($limit)
        ->orderBy($sort_field, $sort_direction)
        ->execute();
  }
}
