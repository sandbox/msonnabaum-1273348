<?php

/**
 * Menu callback; displays a listing of log messages.
 *
 * Messages are truncated at 56 chars. Full-length message could be viewed at
 * the message details page.
 */
function watchdog_ui_overview() {
  $filter = watchdog_ui_build_filter_query();
  $rows = array();
  $classes = array(
    WATCHDOG_DEBUG     => 'watchdog_ui-debug',
    WATCHDOG_INFO      => 'watchdog_ui-info',
    WATCHDOG_NOTICE    => 'watchdog_ui-notice',
    WATCHDOG_WARNING   => 'watchdog_ui-warning',
    WATCHDOG_ERROR     => 'watchdog_ui-error',
    WATCHDOG_CRITICAL  => 'watchdog_ui-critical',
    WATCHDOG_ALERT     => 'watchdog_ui-alert',
    WATCHDOG_EMERGENCY => 'watchdog_ui-emerg',
  );

  $build['watchdog_ui_filter_form'] = drupal_get_form('watchdog_ui_filter_form');
  $build['watchdog_ui_clear_log_form'] = drupal_get_form('watchdog_ui_clear_log_form');

  $header = array(
    '', // Icon column.
    array('data' => t('Type'), 'field' => 'w.type'),
    array('data' => t('Date'), 'field' => 'w.wid', 'sort' => 'desc'),
    t('Message'),
    array('data' => t('User'), 'field' => 'u.name'),
    array('data' => t('Operations')),
  );

  $result = _watchdog_get_object()->getMultiple(50, 'wid', 'DESC');
  foreach ($result as $log) {
    $rows[] = array('data' =>
      array(
        // Cells
        array('class' => 'icon'),
        t($log->type),
        format_date($log->timestamp, 'short'),
        theme('watchdog_ui_message', array('event' => $log, 'link' => TRUE)),
        theme('username', array('account' => $log)),
        filter_xss($log->link),
      ),
      // Attributes for tr
      'class' => array(drupal_html_class('watchdog_ui-' . $log->type), $classes[$log->severity]),
    );
  }

  $build['watchdog_ui_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#attributes' => array('id' => 'admin-watchdog_ui'),
    '#empty' => t('No log messages available.'),
  );
  $build['watchdog_ui_pager'] = array('#theme' => 'pager');

  return $build;
}

/**
 * Menu callback; generic function to display a page of the most frequent events.
 *
 * Messages are not truncated because events from this page have no detail view.
 *
 * @param $type
 *   type of watchdog_ui events to display.
 */
function watchdog_ui_top($type) {

  $header = array(
    array('data' => t('Count'), 'field' => 'count', 'sort' => 'desc'),
    array('data' => t('Message'), 'field' => 'message')
  );
  $count_query = db_select('watchdog');
  $count_query->addExpression('COUNT(DISTINCT(message))');
  $count_query->condition('type', $type);

  $query = db_select('watchdog', 'w')->extend('PagerDefault')->extend('TableSort');
  $query->addExpression('COUNT(wid)', 'count');
  $query = $query
    ->fields('w', array('message', 'variables'))
    ->condition('w.type', $type)
    ->groupBy('message')
    ->groupBy('variables')
    ->limit(30)
    ->orderByHeader($header);
  $query->setCountQuery($count_query);
  $result = $query->execute();

  $rows = array();
  foreach ($result as $watchdog_ui) {
    $rows[] = array($watchdog_ui->count, theme('watchdog_ui_message', array('event' => $watchdog_ui)));
  }

  $build['watchdog_ui_top_table']  = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('No log messages available.'),
  );
  $build['watchdog_ui_top_pager'] = array('#theme' => 'pager');

  return $build;
}

/**
 * Menu callback; displays details about a log message.
 */
function watchdog_ui_event($id) {
  $severity = watchdog_severity_levels();
  $result = _watchdog_get_object()->get($id);
  if ($log = $result) {
    $rows = array(
      array(
        array('data' => t('Type'), 'header' => TRUE),
        t($log->type),
      ),
      array(
        array('data' => t('Date'), 'header' => TRUE),
        format_date($log->timestamp, 'long'),
      ),
      array(
        array('data' => t('User'), 'header' => TRUE),
        theme('username', array('account' => $log)),
      ),
      array(
        array('data' => t('Location'), 'header' => TRUE),
        l($log->location, $log->location),
      ),
      array(
        array('data' => t('Referrer'), 'header' => TRUE),
        l($log->referer, $log->referer),
      ),
      array(
        array('data' => t('Message'), 'header' => TRUE),
        theme('log_message', array('event' => $log)),
      ),
      array(
        array('data' => t('Severity'), 'header' => TRUE),
        $severity[$log->severity],
      ),
      array(
        array('data' => t('Hostname'), 'header' => TRUE),
        check_plain($log->hostname),
      ),
      array(
        array('data' => t('Operations'), 'header' => TRUE),
        $log->link,
      ),
    );
    $build['watchdog_ui_table'] = array(
      '#theme' => 'table',
      '#rows' => $rows,
      '#attributes' => array('class' => array('watchdog_ui-event')),
    );
    return $build;
  }
  else {
    return '';
  }
}

/**
 * Build query for watchdog_ui administration filters based on session.
 */
function watchdog_ui_build_filter_query() {
  if (empty($_SESSION['watchdog_ui_overview_filter'])) {
    return;
  }

  $filters = watchdog_ui_filters();

  // Build query
  $where = $args = array();
  foreach ($_SESSION['watchdog_ui_overview_filter'] as $key => $filter) {
    $filter_where = array();
    foreach ($filter as $value) {
      $filter_where[] = $filters[$key]['where'];
      $args[] = $value;
    }
    if (!empty($filter_where)) {
      $where[] = '(' . implode(' OR ', $filter_where) . ')';
    }
  }
  $where = !empty($where) ? implode(' AND ', $where) : '';

  return array(
    'where' => $where,
    'args' => $args,
  );
}


/**
 * List watchdog_ui administration filters that can be applied.
 */
function watchdog_ui_filters() {
  $filters = array();

  foreach (_watchdog_ui_get_message_types() as $type) {
    $types[$type] = t($type);
  }

  if (!empty($types)) {
    $filters['type'] = array(
      'title' => t('Type'),
      'where' => "w.type = ?",
      'options' => $types,
    );
  }

  $filters['severity'] = array(
    'title' => t('Severity'),
    'where' => 'w.severity = ?',
    'options' => watchdog_severity_levels(),
  );

  return $filters;
}

/**
 * Returns HTML for a log message.
 *
 * @param $variables
 *   An associative array containing:
 *   - event: An object with at least the message and variables properties.
 *   - link: (optional) Format message as link, event->wid is required.
 *
 * @ingroup themeable
 */
function theme_watchdog_ui_message($variables) {
  $output = '';
  $event = $variables['event'];
  // Check for required properties.
  if (isset($event->message) && isset($event->variables)) {
    // Messages without variables or user specified text.
    if ($event->variables === 'N;') {
      $output = $event->message;
    }
    // Message to translate with injected variables.
    else {
      $output = t($event->message, unserialize($event->variables));
    }
    if ($variables['link'] && isset($event->wid)) {
      // Truncate message to 56 chars.
      $output = truncate_utf8(filter_xss($output, array()), 56, TRUE, TRUE);
      $output = l($output, 'admin/reports/event/' . $event->wid, array('html' => TRUE));
    }
  }
  return $output;
}

/**
 * Return form for watchdog_ui administration filters.
 *
 * @ingroup forms
 * @see watchdog_ui_filter_form_submit()
 * @see watchdog_ui_filter_form_validate()
 */
function watchdog_ui_filter_form($form) {
  $filters = watchdog_ui_filters();

  $form['filters'] = array(
    '#type' => 'fieldset',
    '#title' => t('Filter log messages'),
    '#collapsible' => TRUE,
    '#collapsed' => empty($_SESSION['watchdog_ui_overview_filter']),
  );
  foreach ($filters as $key => $filter) {
    $form['filters']['status'][$key] = array(
      '#title' => $filter['title'],
      '#type' => 'select',
      '#multiple' => TRUE,
      '#size' => 8,
      '#options' => $filter['options'],
    );
    if (!empty($_SESSION['watchdog_ui_overview_filter'][$key])) {
      $form['filters']['status'][$key]['#default_value'] = $_SESSION['watchdog_ui_overview_filter'][$key];
    }
  }

  $form['filters']['actions'] = array(
    '#type' => 'actions',
    '#attributes' => array('class' => array('container-inline')),
  );
  $form['filters']['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Filter'),
  );
  if (!empty($_SESSION['watchdog_ui_overview_filter'])) {
    $form['filters']['actions']['reset'] = array(
      '#type' => 'submit',
      '#value' => t('Reset')
    );
  }

  return $form;
}

/**
 * Validate result from watchdog_ui administration filter form.
 */
function watchdog_ui_filter_form_validate($form, &$form_state) {
  if ($form_state['values']['op'] == t('Filter') && empty($form_state['values']['type']) && empty($form_state['values']['severity'])) {
    form_set_error('type', t('You must select something to filter by.'));
  }
}

/**
 * Process result from watchdog_ui administration filter form.
 */
function watchdog_ui_filter_form_submit($form, &$form_state) {
  $op = $form_state['values']['op'];
  $filters = watchdog_ui_filters();
  switch ($op) {
    case t('Filter'):
      foreach ($filters as $name => $filter) {
        if (isset($form_state['values'][$name])) {
          $_SESSION['watchdog_ui_overview_filter'][$name] = $form_state['values'][$name];
        }
      }
      break;
    case t('Reset'):
      $_SESSION['watchdog_ui_overview_filter'] = array();
      break;
  }
  return 'admin/reports/watchdog_ui';
}

/**
 * Return form for watchdog_ui clear button.
 *
 * @ingroup forms
 * @see watchdog_ui_clear_log_submit()
 */
function watchdog_ui_clear_log_form($form) {
  $form['watchdog_ui_clear'] = array(
    '#type' => 'fieldset',
    '#title' => t('Clear log messages'),
    '#description' => t('This will permanently remove the log messages from the database.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['watchdog_ui_clear']['clear'] = array(
    '#type' => 'submit',
    '#value' => t('Clear log messages'),
    '#submit' => array('watchdog_ui_clear_log_submit'),
  );

  return $form;
}

/**
 * Submit callback: clear database with log messages.
 */
function watchdog_ui_clear_log_submit() {
  $_SESSION['watchdog_ui_overview_filter'] = array();
  db_delete('watchdog')->execute();
  drupal_set_message(t('Database log cleared.'));
}
