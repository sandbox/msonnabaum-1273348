<?php
/**
 * Created by JetBrains PhpStorm.
 * User: msonnabaum
 * Date: 8/27/11
 * Time: 21:29 
 * To change this template use File | Settings | File Templates.
 */

interface WatchdogInterface {

  public function getMessageTypes();
  public function log(array $log_entry);
  public function get($wid);
  public function getMultiple($limit = 50, $sort_field = 'wid', $sort_direction = 'DESC');
}
